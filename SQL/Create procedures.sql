USE [dbXcaret]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetActivities]    Script Date: 21/02/2020 01:19:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*
[dbo].[sp_GetActivities] 1
*/
CREATE PROCEDURE [dbo].[sp_GetActivities]
	@BussinesUnitID int 
AS
BEGIN
	select 
		ID_Actividad as ID
		, a.Nombre as Name
		, a.ID_Unidad_Negocio as BussinesUnitID
		, a.Descripcion as Description
		, a.Geo_Loc_Lat as GeoLocLat
		, a.Geo_Loc_Long as GeoLocLong
		, c.ID_Categoria as CategoryId
		, c.Nombre as CategoryName
		, s.ID_Subcategoria as SubCatID
		, s.Nombre as SubCatName
	from actividades a
		inner join categorias c on a.ID_Categoria = c.ID_Categoria
		left join subcategorias s on a.ID_Subcategoria = s.ID_Subcategoria
	where a.status = 1 and a.ID_Unidad_Negocio = @BussinesUnitID
END
GO

/****** Object:  StoredProcedure [dbo].[sp_GetActivitiesWithAuthorizationPending]    Script Date: 21/02/2020 01:19:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*
[dbo].[sp_GetActivitiesWithAuthorizationPending] 1
*/
CREATE PROCEDURE [dbo].[sp_GetActivitiesWithAuthorizationPending]
	@BussinesUnitID int = null
AS
BEGIN
	select 
		ab.ID_Actividad_Bitacora as RequestID
		, ab.ID_Actividad as ActivityID
		, ab.Nombre as ActivityName

		, ab.ID_Unidad_Negocio as BussinesUnitID
		, un.Nombre as BussinesUnitName

		, ab.Bitacora_Usuario_Alta as RequestUserID
		, ur.Nombre as RequestUserName

		, ab.Bitacora_Fecha	as RequestDate
		, ab.Bitacora_Status as AuthStatus
		, ab.Bitacora_Accion as ActionID
		, case when ab.Bitacora_Accion = 1 then 'NUEVA'
				when ab.Bitacora_Accion = 2 then 'ACTUALIZAR'
				when ab.Bitacora_Accion = 3 then 'ELIMINAR' -- Actualizar status a 0 
				end as ActionName


	from Actividades_Bitacora ab
		left join Unidad_Negocio un on ab.ID_Unidad_Negocio = un.ID_Unidad_Negocio
		inner join Usuarios ur on ab.Bitacora_Usuario_Alta = ur.ID_Usuario
	where ab.Bitacora_Status = 0 
		and (@BussinesUnitID is null or ab.ID_Unidad_Negocio = @BussinesUnitID)
END
GO

/****** Object:  StoredProcedure [dbo].[sp_GetActivityDetail]    Script Date: 21/02/2020 01:19:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*
sp_GetActivityDetail 2
*/
CREATE PROCEDURE [dbo].[sp_GetActivityDetail]
	@ActivityID int 
AS
BEGIN
	select 
		ID_Actividad as ID
		, a.Nombre as Name
		, un.ID_Unidad_Negocio as BussinesUnitID
		, un.Nombre as BussinesUnitName
		, a.Descripcion as Description
		, convert(varchar(100), a.Geo_Loc_Lat) as GeoLocLat
		, convert(varchar(100), a.Geo_Loc_Long) as GeoLocLong
		, a.[Status]
		, c.ID_Categoria as CategoryId
		, c.Nombre as CategoryName
		, s.ID_Subcategoria as SubCatID
		, s.Nombre as SubCatName
	from actividades a
		inner join Unidad_Negocio un on a.ID_Unidad_Negocio = un.ID_Unidad_Negocio
		inner join categorias c on a.ID_Categoria = c.ID_Categoria
		left join subcategorias s on a.ID_Subcategoria = s.ID_Subcategoria
	where a.ID_Actividad = @ActivityID
END
GO

/****** Object:  StoredProcedure [dbo].[sp_GetCategories]    Script Date: 21/02/2020 01:19:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetCategories]

AS
BEGIN
	select c.ID_Categoria as ID
		, c.Nombre as Name
	from Categorias c
END
GO

/****** Object:  StoredProcedure [dbo].[sp_GetSubcategories]    Script Date: 21/02/2020 01:19:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetSubcategories]
	@ID_Category int 
AS
BEGIN
	select s.ID_Subcategoria as ID
		, s.Nombre as Name
	from Subcategorias s
	where s.ID_Categoria = @ID_Category
END
GO

/****** Object:  StoredProcedure [dbo].[sp_InsertActivity]    Script Date: 21/02/2020 01:19:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertActivity] 
	@Name varchar(100)
	, @BussinesUnitID int
	, @Description varchar(500)
	, @GeoLocLat float
	, @GeoLocLong float
	, @CategoryID int
	, @SubcategoryID int = NULL
	, @UserID int
AS
BEGIN
	declare @UserSupervisor bit = 0

	-- TODO: select para validar que el usuario es supervisor
	if exists(select ID_Usuario from usuarios where ID_Usuario = @UserID and ID_Tipo_Usuario = 1)
	begin
		set @UserSupervisor = 1
	end


	if(@UserSupervisor = 0)
	begin 

	INSERT INTO Actividades_Bitacora (
		 Nombre
		, ID_Unidad_Negocio
		, Descripcion
		, Geo_Loc_Lat
		, Geo_Loc_Long
		, [Status]
		, ID_Categoria
		, ID_Subcategoria
		, Bitacora_Fecha
        , Bitacora_Usuario_Alta
        , Bitacora_Accion
		)
	VALUES (
		@Name
		, @BussinesUnitID
		, @Description 
		, @GeoLocLat 
		, @GeoLocLong 
		, 1
		, @CategoryId 
		, @SubcategoryID
		, GETDATE()
		, @UserID
		, 1 -- 1 Insert, 2 Update, 3 Delete
		)
	end
	else 
	begin
		
		INSERT INTO Actividades (
			 Nombre
			, ID_Unidad_Negocio
			, Descripcion
			, Geo_Loc_Lat
			, Geo_Loc_Long
			, [Status]
			, ID_Categoria
			, ID_Subcategoria
			)
		VALUES (
			@Name
			, @BussinesUnitID
			, @Description 
			, @GeoLocLat 
			, @GeoLocLong 
			, 1
			, @CategoryId 
			, @SubcategoryID
			)

		declare @ActivityID int = @@Identity

		INSERT INTO Actividades_Bitacora (
			 ID_Actividad
			, Nombre
			, ID_Unidad_Negocio
			, Descripcion
			, Geo_Loc_Lat
			, Geo_Loc_Long
			, [Status]
			, ID_Categoria
			, ID_Subcategoria
			, Bitacora_Fecha
			, Bitacora_Usuario_Alta
			, Bitacora_Accion
			, Bitacora_Usuario_Supervisor
			, Bitacora_Status
			)
		VALUES (
			@ActivityID
			, @Name
			, @BussinesUnitID
			, @Description 
			, @GeoLocLat 
			, @GeoLocLong 
			, 1
			, @CategoryId 
			, @SubcategoryID
			, GETDATE()
			, @UserID
			, 1 -- 1 Insert, 2 Update, 3 Delete
			, @UserID
			, 1 -- 0 Pendiente, 1 Aprobada, 2 Rechazada
			)


	end

END
GO

/****** Object:  StoredProcedure [dbo].[sp_UpdateActivity]    Script Date: 21/02/2020 01:19:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_UpdateActivity] 
	@ActivityID int
	, @Name varchar(100)
	, @BussinesUnitID int
	, @Description varchar(500)
	, @GeoLocLat float
	, @GeoLocLong float
	, @CategoryID int
	, @SubcategoryID int = NULL
	, @Status int 
	, @UserID int
AS
BEGIN
	declare @UserSupervisor bit = 0

	-- TODO: select para validar que el usuario es supervisor
	if exists(select ID_Usuario from usuarios where ID_Usuario = @UserID and ID_Tipo_Usuario = 1)
	begin
		set @UserSupervisor = 1
	end

	if(@UserSupervisor = 0)
	begin 

	INSERT INTO Actividades_Bitacora (
		ID_Actividad
		, Nombre
		, ID_Unidad_Negocio
		, Descripcion
		, Geo_Loc_Lat
		, Geo_Loc_Long
		, [Status]
		, ID_Categoria
		, ID_Subcategoria
		, Bitacora_Fecha
        , Bitacora_Usuario_Alta
        , Bitacora_Accion
		)
	VALUES (
		@ActivityID
		, @Name
		, @BussinesUnitID
		, @Description 
		, @GeoLocLat 
		, @GeoLocLong 
		, @Status
		, @CategoryId 
		, @SubcategoryID
		, GETDATE()
		, @UserID
		, 2 -- 1 Insert, 2 Update, 3 Delete
		)
	end
	else 
	begin
		
		UPDATE Actividades 
		set Nombre = @Name 
			, ID_Unidad_Negocio = @BussinesUnitID
			, Descripcion = @Description
			, Geo_Loc_Lat = @GeoLocLat
			, Geo_Loc_Long = @GeoLocLong
			, [Status] = @Status
			, ID_Categoria = @CategoryId
			, ID_Subcategoria = @SubcategoryID
		where ID_Actividad = @ActivityID

		INSERT INTO Actividades_Bitacora (
			 ID_Actividad
			, Nombre
			, ID_Unidad_Negocio
			, Descripcion
			, Geo_Loc_Lat
			, Geo_Loc_Long
			, [Status]
			, ID_Categoria
			, ID_Subcategoria
			, Bitacora_Fecha
			, Bitacora_Usuario_Alta
			, Bitacora_Accion
			, Bitacora_Usuario_Supervisor
			, Bitacora_Status
			)
		VALUES (
			@ActivityID
			, @Name
			, @BussinesUnitID
			, @Description 
			, @GeoLocLat 
			, @GeoLocLong 
			, @Status
			, @CategoryId 
			, @SubcategoryID
			, GETDATE()
			, @UserID
			, 2 -- 1 Insert, 2 Update, 3 Delete
			, @UserID
			, 1 -- 0 Pendiente, 1 Aprobada, 2 Rechazada
			)


	end

END
GO

/****** Object:  StoredProcedure [dbo].[sp_UpdateActivityBinnacle]    Script Date: 21/02/2020 01:19:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_UpdateActivityBinnacle]
	@RequestID int
	, @UserID int
	, @BinnacleStatusID int
AS
BEGIN

	declare @ActionId int 
	, @ActivityID int
	, @Name varchar(100)
	, @BussinesUnitID int
	, @Description varchar(500)
	, @GeoLocLat float
	, @GeoLocLong float
	, @CategoryID int
	, @SubcategoryID int = NULL
	, @Status int 

	-- Validar que la solicitud no ha sido actualizada
	if not exists(select ID_Actividad_Bitacora from Actividades_Bitacora where ID_Actividad_Bitacora = @RequestID and Bitacora_Status = 0)
	begin
		return
	end

	select @ActionId = ab.Bitacora_Accion from Actividades_Bitacora ab where ID_Actividad_Bitacora = @RequestID
	
	if @BinnacleStatusID = 1 -- Si se aprueba la solicitud
	begin
		if @ActionId = 1 -- NUEVA
		begin

			INSERT INTO Actividades (
				 Nombre
				, ID_Unidad_Negocio
				, Descripcion
				, Geo_Loc_Lat
				, Geo_Loc_Long
				, [Status]
				, ID_Categoria
				, ID_Subcategoria
				)
			select  Nombre
				, ID_Unidad_Negocio
				, Descripcion
				, Geo_Loc_Lat
				, Geo_Loc_Long
				, [Status]
				, ID_Categoria
				, ID_Subcategoria
			from Actividades_Bitacora 
			where ID_Actividad_Bitacora = @RequestID

		end
		else if  @ActionId = 2 -- ACTUALIZAR
		begin
			select
				@ActivityID = ID_Actividad
				, @Name = Nombre 
				, @BussinesUnitID = ID_Unidad_Negocio
				, @Description = Descripcion
				, @GeoLocLat = Geo_Loc_Lat
				, @GeoLocLong = Geo_Loc_Long
				, @Status = [Status]
				, @CategoryId = ID_Categoria
				, @SubcategoryID = ID_Subcategoria
			from Actividades_Bitacora 
			where ID_Actividad_Bitacora = @RequestID

			UPDATE Actividades 
			set Nombre = @Name 
				, ID_Unidad_Negocio = @BussinesUnitID
				, Descripcion = @Description
				, Geo_Loc_Lat = @GeoLocLat
				, Geo_Loc_Long = @GeoLocLong
				, [Status] = @Status
				, ID_Categoria = @CategoryId
				, ID_Subcategoria = @SubcategoryID
			where ID_Actividad = @ActivityID

		end
		else if  @ActionId = 3 -- ELIMINAR
		begin
			select
				@ActivityID = ID_Actividad
			from Actividades_Bitacora 
			where ID_Actividad_Bitacora = @RequestID

			UPDATE Actividades 
			set [Status] = 0
			where ID_Actividad = @ActivityID
		end
	end

	-- Actualiza estatus y usuario que modifica la bitacora
	update Actividades_Bitacora
	set Bitacora_Status = @BinnacleStatusID
		, Bitacora_Usuario_Supervisor = @UserID
	where ID_Actividad_Bitacora = @RequestID


END
GO


