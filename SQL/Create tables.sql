USE [dbXcaret]
GO

/****** Object:  Table [dbo].[Actividades]    Script Date: 21/02/2020 01:20:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Actividades](
	[ID_Actividad] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[ID_Unidad_Negocio] [int] NOT NULL,
	[Descripcion] [varchar](1000) NOT NULL,
	[Geo_Loc_Lat] [float] NOT NULL,
	[Geo_Loc_Long] [float] NOT NULL,
	[Status] [int] NOT NULL,
	[ID_Categoria] [int] NOT NULL,
	[ID_Subcategoria] [int] NULL,
 CONSTRAINT [PK_Actividades] PRIMARY KEY CLUSTERED 
(
	[ID_Actividad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Actividades_Bitacora]    Script Date: 21/02/2020 01:20:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Actividades_Bitacora](
	[ID_Actividad_Bitacora] [int] IDENTITY(1,1) NOT NULL,
	[ID_Actividad] [int] NULL,
	[Nombre] [varchar](100) NOT NULL,
	[ID_Unidad_Negocio] [int] NOT NULL,
	[Descripcion] [varchar](1000) NOT NULL,
	[Geo_Loc_Lat] [float] NOT NULL,
	[Geo_Loc_Long] [float] NOT NULL,
	[Status] [int] NOT NULL,
	[ID_Categoria] [int] NOT NULL,
	[ID_Subcategoria] [int] NULL,
	[Bitacora_Fecha] [date] NOT NULL,
	[Bitacora_Usuario_Alta] [int] NOT NULL,
	[Bitacora_Accion] [int] NOT NULL,
	[Bitacora_Usuario_Supervisor] [int] NULL,
	[Bitacora_Status] [int] NOT NULL,
 CONSTRAINT [PK_Actividades_Bitacora] PRIMARY KEY CLUSTERED 
(
	[ID_Actividad_Bitacora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Actividades_Horarios]    Script Date: 21/02/2020 01:20:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Actividades_Horarios](
	[ID_Actividad_Horario] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[ID_Actividad] [int] NOT NULL,
	[ID_Temporada] [int] NULL,
	[Hora_inicio] [time](7) NOT NULL,
	[Hora_fin] [time](7) NOT NULL,
 CONSTRAINT [PK_Actividades_Horarios] PRIMARY KEY CLUSTERED 
(
	[ID_Actividad_Horario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Actividades_Restricciones]    Script Date: 21/02/2020 01:20:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Actividades_Restricciones](
	[ID_Actividad_Restriccion] [int] IDENTITY(1,1) NOT NULL,
	[ID_Actividad] [int] NOT NULL,
	[ID_Restriccion] [int] NOT NULL,
 CONSTRAINT [PK_Actividades_Restricciones] PRIMARY KEY CLUSTERED 
(
	[ID_Actividad_Restriccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Categorias]    Script Date: 21/02/2020 01:20:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Categorias](
	[ID_Categoria] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
 CONSTRAINT [PK_Categorias] PRIMARY KEY CLUSTERED 
(
	[ID_Categoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Restricciones]    Script Date: 21/02/2020 01:20:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Restricciones](
	[ID_Restriccion] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Restricciones] PRIMARY KEY CLUSTERED 
(
	[ID_Restriccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Subcategorias]    Script Date: 21/02/2020 01:20:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Subcategorias](
	[ID_Subcategoria] [int] IDENTITY(1,1) NOT NULL,
	[ID_Categoria] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
 CONSTRAINT [PK_Subcategorias] PRIMARY KEY CLUSTERED 
(
	[ID_Subcategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Temporadas]    Script Date: 21/02/2020 01:20:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Temporadas](
	[ID_Temporada] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Fecha_inicio] [date] NOT NULL,
	[Fecha_fin] [date] NOT NULL,
 CONSTRAINT [PK_Temporadas] PRIMARY KEY CLUSTERED 
(
	[ID_Temporada] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Unidad_Negocio]    Script Date: 21/02/2020 01:20:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Unidad_Negocio](
	[ID_Unidad_Negocio] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Tipo] [int] NOT NULL,
 CONSTRAINT [PK_Unidad_Negocio] PRIMARY KEY CLUSTERED 
(
	[ID_Unidad_Negocio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Usuarios]    Script Date: 21/02/2020 01:20:48 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Usuarios](
	[ID_Usuario] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Clave] [varchar](100) NOT NULL,
	[ID_Tipo_Usuario] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Actividades_Bitacora] ADD  CONSTRAINT [DF__Actividad__Bitac__72C60C4A]  DEFAULT (getdate()) FOR [Bitacora_Fecha]
GO

ALTER TABLE [dbo].[Actividades_Bitacora] ADD  CONSTRAINT [DF__Actividad__Bitac__73BA3083]  DEFAULT ((0)) FOR [Bitacora_Status]
GO

ALTER TABLE [dbo].[Actividades]  WITH CHECK ADD  CONSTRAINT [FK_Actividades_Categorias] FOREIGN KEY([ID_Categoria])
REFERENCES [dbo].[Categorias] ([ID_Categoria])
GO

ALTER TABLE [dbo].[Actividades] CHECK CONSTRAINT [FK_Actividades_Categorias]
GO

ALTER TABLE [dbo].[Actividades]  WITH CHECK ADD  CONSTRAINT [FK_Actividades_Subcategorias] FOREIGN KEY([ID_Subcategoria])
REFERENCES [dbo].[Subcategorias] ([ID_Subcategoria])
GO

ALTER TABLE [dbo].[Actividades] CHECK CONSTRAINT [FK_Actividades_Subcategorias]
GO

ALTER TABLE [dbo].[Actividades]  WITH CHECK ADD  CONSTRAINT [FK_Actividades_Unidad_Negocio] FOREIGN KEY([ID_Unidad_Negocio])
REFERENCES [dbo].[Unidad_Negocio] ([ID_Unidad_Negocio])
GO

ALTER TABLE [dbo].[Actividades] CHECK CONSTRAINT [FK_Actividades_Unidad_Negocio]
GO

ALTER TABLE [dbo].[Actividades_Horarios]  WITH CHECK ADD  CONSTRAINT [FK_Actividades_Horarios_Actividades] FOREIGN KEY([ID_Actividad])
REFERENCES [dbo].[Actividades] ([ID_Actividad])
GO

ALTER TABLE [dbo].[Actividades_Horarios] CHECK CONSTRAINT [FK_Actividades_Horarios_Actividades]
GO

ALTER TABLE [dbo].[Actividades_Horarios]  WITH CHECK ADD  CONSTRAINT [FK_Actividades_Horarios_Temporadas] FOREIGN KEY([ID_Temporada])
REFERENCES [dbo].[Temporadas] ([ID_Temporada])
GO

ALTER TABLE [dbo].[Actividades_Horarios] CHECK CONSTRAINT [FK_Actividades_Horarios_Temporadas]
GO

ALTER TABLE [dbo].[Actividades_Restricciones]  WITH CHECK ADD  CONSTRAINT [FK_Actividades_Restricciones_Actividades] FOREIGN KEY([ID_Actividad])
REFERENCES [dbo].[Actividades] ([ID_Actividad])
GO

ALTER TABLE [dbo].[Actividades_Restricciones] CHECK CONSTRAINT [FK_Actividades_Restricciones_Actividades]
GO

ALTER TABLE [dbo].[Actividades_Restricciones]  WITH CHECK ADD  CONSTRAINT [FK_Actividades_Restricciones_Restricciones] FOREIGN KEY([ID_Restriccion])
REFERENCES [dbo].[Restricciones] ([ID_Restriccion])
GO

ALTER TABLE [dbo].[Actividades_Restricciones] CHECK CONSTRAINT [FK_Actividades_Restricciones_Restricciones]
GO


