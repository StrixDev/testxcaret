﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

using Microsoft.Extensions.Configuration;

using TextXcaretWebApp.Models;

namespace TextXcaretWebApp.DataAccess
{
	public class daoActivities
	{
		public IConfiguration Configuration { get; }
		public daoActivities(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		private string GetConnectionString()
		{
			string connectionString = Configuration.GetConnectionString("DefaultConnection");
			return connectionString;
		}

		public List<ActivityModel> SelectActivityList(int businesUnitId)
		{
			List<ActivityModel> activityList = new List<ActivityModel>();

			try
			{
				using (SqlConnection connection = new SqlConnection(GetConnectionString()))
				{
					connection.Open();
					SqlCommand command = new SqlCommand
					{
						Connection = connection,
						CommandText = "sp_GetActivities",
						CommandType = System.Data.CommandType.StoredProcedure
					};

					command.Parameters.Add(new SqlParameter("BussinesUnitID", businesUnitId));

					using (SqlDataReader dataReader = command.ExecuteReader())
					{
						if (dataReader.HasRows)
						{
							while (dataReader.Read())
							{
								ActivityModel activityData = new ActivityModel();

								activityData.ID = dataReader["ID"] as int? ?? default(int);
								activityData.Name = dataReader["Name"] as string ?? string.Empty;

								activityData.BussinesUnitID = dataReader["BussinesUnitID"] as int? ?? default(int);

								activityData.Description = dataReader["Description"] as string ?? string.Empty;
								activityData.GeoLocLat = dataReader["GeoLocLat"] as float? ?? default(int); 
								activityData.GeoLocLong = dataReader["GeoLocLong"] as float? ?? default(int); 

								activityData.CategoryId = dataReader["CategoryId"] as int? ?? default(int);
								activityData.CategoryName = dataReader["CategoryName"] as string ?? string.Empty;

								activityData.SubCatID = dataReader["SubCatID"] as int? ?? default(int); 
								activityData.SubCatName = dataReader["SubCatID"] as string ?? string.Empty;

								activityList.Add(activityData);
							}
						}
					}
					connection.Close();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return activityList;
		}

		public ActivityViewModel SelectActivityDetail(int activityId)
		{
			ActivityViewModel activityDetail = new ActivityViewModel();

			try
			{
				using (SqlConnection connection = new SqlConnection(GetConnectionString()))
				{
					connection.Open();
					SqlCommand command = new SqlCommand
					{
						Connection = connection,
						CommandText = "sp_GetActivityDetail",
						CommandType = System.Data.CommandType.StoredProcedure
					};

					command.Parameters.Add(new SqlParameter("ActivityId", activityId));

					using (SqlDataReader dataReader = command.ExecuteReader())
					{
						if (dataReader.HasRows)
						{
							while (dataReader.Read())
							{
								activityDetail.ID = dataReader["ID"] as int? ?? default;
								activityDetail.Name = dataReader["Name"] as string ?? string.Empty;

								activityDetail.BussinesUnitID = dataReader["BussinesUnitID"] as int? ?? default;
								activityDetail.BussinesUnitName = dataReader["BussinesUnitName"] as string ?? string.Empty;

								activityDetail.Description = dataReader["Description"] as string ?? string.Empty;
								activityDetail.GeoLocLat = (float)Convert.ToDouble( dataReader["GeoLocLat"]);
								activityDetail.GeoLocLong = (float)Convert.ToDouble( dataReader["GeoLocLong"]);
								activityDetail.Status = dataReader["Status"] as int? ?? default;

								activityDetail.CategoryId = dataReader["CategoryId"] as int? ?? default;
								activityDetail.CategoryName = dataReader["CategoryName"] as string ?? string.Empty;

								activityDetail.SubCatID = dataReader["SubCatID"] as int? ?? default;
								activityDetail.SubCatName = dataReader["SubCatID"] as string ?? string.Empty;

							}
						}
					}
					connection.Close();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return activityDetail;
		}


		public bool InsertActivity(ActivityModel activityModel, int userID)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(GetConnectionString()))
				{
					connection.Open();
					SqlCommand command = new SqlCommand
					{
						Connection = connection,
						CommandText = "sp_InsertActivity",
						CommandType = System.Data.CommandType.StoredProcedure
					};

					command.Parameters.Add(new SqlParameter() { ParameterName = "Name", SqlDbType = System.Data.SqlDbType.VarChar, Value = activityModel.Name });
					command.Parameters.Add(new SqlParameter() { ParameterName = "BussinesUnitID", SqlDbType = System.Data.SqlDbType.Int, Value = activityModel.BussinesUnitID });
					command.Parameters.Add(new SqlParameter() { ParameterName = "Description", SqlDbType = System.Data.SqlDbType.VarChar, Value = activityModel.Description });
					command.Parameters.Add(new SqlParameter() { ParameterName = "GeoLocLat", SqlDbType = System.Data.SqlDbType.Float, Value = activityModel.GeoLocLat });
					command.Parameters.Add(new SqlParameter() { ParameterName = "GeoLocLong", SqlDbType = System.Data.SqlDbType.Float, Value = activityModel.GeoLocLong });
					command.Parameters.Add(new SqlParameter() { ParameterName = "CategoryId", SqlDbType = System.Data.SqlDbType.Int, Value = activityModel.CategoryId });
					command.Parameters.Add(new SqlParameter() { ParameterName = "UserID", SqlDbType = System.Data.SqlDbType.Int, Value = userID });

					if (activityModel.SubCatID != 0)
					{
						command.Parameters.Add(new SqlParameter() { ParameterName = "SubcategoryID", SqlDbType = System.Data.SqlDbType.Int, Value = activityModel.SubCatID });	
					}

					int rows = command.ExecuteNonQuery();
					connection.Close();

					return (rows != 0 && rows == 1);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool UpdateActivity(ActivityModel activityModel, int userID)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(GetConnectionString()))
				{
					connection.Open();
					SqlCommand command = new SqlCommand
					{
						Connection = connection,
						CommandText = "sp_UpdateActivity",
						CommandType = System.Data.CommandType.StoredProcedure
					};

					command.Parameters.Add(new SqlParameter() { ParameterName = "ActivityID", SqlDbType = System.Data.SqlDbType.VarChar, Value = activityModel.ID });
					command.Parameters.Add(new SqlParameter() { ParameterName = "Name", SqlDbType = System.Data.SqlDbType.VarChar, Value = activityModel.Name });
					command.Parameters.Add(new SqlParameter() { ParameterName = "BussinesUnitID", SqlDbType = System.Data.SqlDbType.Int, Value = activityModel.BussinesUnitID });
					command.Parameters.Add(new SqlParameter() { ParameterName = "Description", SqlDbType = System.Data.SqlDbType.VarChar, Value = activityModel.Description });
					command.Parameters.Add(new SqlParameter() { ParameterName = "GeoLocLat", SqlDbType = System.Data.SqlDbType.Float, Value = activityModel.GeoLocLat });
					command.Parameters.Add(new SqlParameter() { ParameterName = "GeoLocLong", SqlDbType = System.Data.SqlDbType.Float, Value = activityModel.GeoLocLong });
					command.Parameters.Add(new SqlParameter() { ParameterName = "CategoryId", SqlDbType = System.Data.SqlDbType.Int, Value = activityModel.CategoryId });
					command.Parameters.Add(new SqlParameter() { ParameterName = "Status", SqlDbType = System.Data.SqlDbType.Int, Value = activityModel.Status });
					command.Parameters.Add(new SqlParameter() { ParameterName = "UserID", SqlDbType = System.Data.SqlDbType.Int, Value = userID });

					if (activityModel.SubCatID != 0)
					{
						command.Parameters.Add(new SqlParameter() { ParameterName = "SubcategoryID", SqlDbType = System.Data.SqlDbType.Int, Value = activityModel.SubCatID });
					}

					int rows = command.ExecuteNonQuery();
					connection.Close();

					return (rows != 0 && rows == 1);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

	}
}
