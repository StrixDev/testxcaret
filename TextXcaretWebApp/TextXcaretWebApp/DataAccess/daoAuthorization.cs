﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

using Microsoft.Extensions.Configuration;

using TextXcaretWebApp.Models;

namespace TextXcaretWebApp.DataAccess
{
	public class daoAuthorization
	{
		public IConfiguration Configuration { get; }
		public daoAuthorization(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		private string GetConnectionString()
		{
			string connectionString = Configuration.GetConnectionString("DefaultConnection");
			return connectionString;
		}

		public List<AuthorizationActivityModel> SelectActivityList(int? businesUnitId)
		{
			List<AuthorizationActivityModel> activityList = new List<AuthorizationActivityModel>();

			try
			{
				using (SqlConnection connection = new SqlConnection(GetConnectionString()))
				{
					connection.Open();
					SqlCommand command = new SqlCommand
					{
						Connection = connection,
						CommandText = "sp_GetActivitiesWithAuthorizationPending",
						CommandType = System.Data.CommandType.StoredProcedure
					};

					if (businesUnitId != null)
					{
						command.Parameters.Add(new SqlParameter("BussinesUnitID", businesUnitId));
					}

					using (SqlDataReader dataReader = command.ExecuteReader())
					{
						if (dataReader.HasRows)
						{
							while (dataReader.Read())
							{
								AuthorizationActivityModel activityData = new AuthorizationActivityModel();

								activityData.RequestID = dataReader["RequestID"] as int? ?? default;

								activityData.ID = dataReader["ActivityID"] as int? ?? default;
								activityData.Name = dataReader["ActivityName"] as string ?? string.Empty;

								activityData.BussinesUnitID = dataReader["BussinesUnitID"] as int? ?? default;
								activityData.BussinesUnitName = dataReader["BussinesUnitName"] as string ?? string.Empty;

								activityData.ActionID = dataReader["ActionID"] as int? ?? default;
								activityData.ActionName = dataReader["ActionName"] as string ?? string.Empty;

								activityData.RequestDate = dataReader["RequestDate"] as DateTime? ?? default;
								activityData.AuthStatus = dataReader["AuthStatus"] as int? ?? default;

								activityList.Add(activityData);
							}
						}
					}
					connection.Close();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return activityList;
		}

		public bool UpdateActivity(int requestID, int userID, int binnacleStatusID)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(GetConnectionString()))
				{
					connection.Open();
					SqlCommand command = new SqlCommand
					{
						Connection = connection,
						CommandText = "sp_UpdateActivityBinnacle",
						CommandType = System.Data.CommandType.StoredProcedure
					};

					command.Parameters.Add(new SqlParameter() { ParameterName = "RequestID", SqlDbType = System.Data.SqlDbType.Int, Value = requestID });
					command.Parameters.Add(new SqlParameter() { ParameterName = "UserID", SqlDbType = System.Data.SqlDbType.Int, Value = userID });
					command.Parameters.Add(new SqlParameter() { ParameterName = "BinnacleStatusID", SqlDbType = System.Data.SqlDbType.Int, Value = binnacleStatusID });

					int rows = command.ExecuteNonQuery();
					connection.Close();

					return (rows != 0 && rows == 1);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
