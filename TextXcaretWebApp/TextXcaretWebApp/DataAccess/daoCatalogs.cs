﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

using Microsoft.Extensions.Configuration;

using TextXcaretWebApp.Models;
namespace TextXcaretWebApp.DataAccess
{
	public class daoCatalogs
	{
		public IConfiguration Configuration { get; }
		public daoCatalogs(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		private string GetConnectionString()
		{
			string connectionString = Configuration.GetConnectionString("DefaultConnection");
			return connectionString;
		}

		public List<CategoryModel> SelectCategoryList()
		{
			List<CategoryModel> categoryList = new List<CategoryModel>();
			   		 
			try
			{
				using (SqlConnection connection = new SqlConnection(GetConnectionString()))
				{
					connection.Open();
					SqlCommand command = new SqlCommand
					{
						Connection = connection,
						CommandText = "sp_GetCategories",
						CommandType = System.Data.CommandType.StoredProcedure
					};

					using (SqlDataReader dataReader = command.ExecuteReader())
					{
						if (dataReader.HasRows)
						{
							while (dataReader.Read())
							{
								CategoryModel data = new CategoryModel();

								data.ID = dataReader["ID"] as int? ?? default;
								data.Name = dataReader["Name"] as string ?? string.Empty;

								categoryList.Add(data);
							}
						}
					}
					connection.Close();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return categoryList;
		}

		public List<SubcategoryModel> SelectSubcategoryList(int categoryId)
		{
			List<SubcategoryModel> subcategoryList = new List<SubcategoryModel>();

			try
			{
				using (SqlConnection connection = new SqlConnection(GetConnectionString()))
				{
					connection.Open();
					SqlCommand command = new SqlCommand
					{
						Connection = connection,
						CommandText = "sp_GetSubcategories",
						CommandType = System.Data.CommandType.StoredProcedure
					};

					command.Parameters.Add(new SqlParameter("ID_Category", categoryId));

					using (SqlDataReader dataReader = command.ExecuteReader())
					{
						if (dataReader.HasRows)
						{
							while (dataReader.Read())
							{
								SubcategoryModel data = new SubcategoryModel();

								data.ID = dataReader["ID"] as int? ?? default;
								data.Name = dataReader["Name"] as string ?? string.Empty;

								subcategoryList.Add(data);
							}
						}
					}
					connection.Close();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return subcategoryList;
		}


	}
}
