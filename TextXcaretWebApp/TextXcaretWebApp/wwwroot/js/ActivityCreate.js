﻿function getSubcategories() {
	$('#ddlSubcategory').empty();
	$('#ddlSubcategory').append('<option value="0">Selecciona una opción</option>');

	var selectedCategory = $('#ddlCategory').val();

	var url = "/Activities/GetSubcategoryList?categoryId=" + selectedCategory;

	$.ajax({
		url: url
		, contentType: "Application/Json,charset=utf-8"
		, dataType: "Json"
		, success: function (data) {
			for (var i = 0; i < data.lenght; i++) {
				$('#ddlSubcategory').append(
					'<option value="' + data[i].ID + '">' + data[i].Name + '</option>'
				);
			}
			
		}
	});
}

function setCategory() {
	$('#hidenCategoryId').val($('#ddlCategory').val());
}

function setSubcategory() {
	$('#hidenSubcategoryId').val($('#ddlSubcategory').val());
}