﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextXcaretWebApp.Models
{
	public class ActivityModel
	{
		public int? ID { get; set; }
		public string Name { get; set; }

		public int? BussinesUnitID { get; set; }

		public string BussinesUnitName { get; set; }

		public string Description { get; set; }

		public float? GeoLocLat { get; set; }

		public float? GeoLocLong { get; set; }

		public int? Status { get; set; }

		public string CategoryName { get; set; }

		public int? CategoryId { get; set; }

		public string SubCatName { get; set; }

		public int? SubCatID { get; set; }

	}
}
