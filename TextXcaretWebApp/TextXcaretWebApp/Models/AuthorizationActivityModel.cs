﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextXcaretWebApp.Models
{
	public class AuthorizationActivityModel : ActivityModel
	{
		public int RequestID { get; set; }

		public int? RequestUserID { get; set; }

		public string RequestUserName { get; set; }

		public DateTime? RequestDate { get; set; }

		public int? ActionID { get; set; }

		public string ActionName { get; set; }

		public int? AuthStatus { get; set; }

		public int? AuthUserID { get; set; }

		public string AuthUserName { get; set; }

	}
}
