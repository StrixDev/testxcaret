﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextXcaretWebApp.Models
{
	public class ServiceModel
	{
		public int ID { get; set; }

		public string Name { get; set; }

		public int ID_Unidad_Negocio { get; set; }

		public int ID_Tipo_Servicio { get; set; }

		public float Geo_Loc_Lat { get; set; }

		public float Geo_Loc_Long { get; set; }

		public int Status { get; set; }
	}
}
