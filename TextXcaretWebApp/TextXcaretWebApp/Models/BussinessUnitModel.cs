﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TextXcaretWebApp.Enums;

namespace TextXcaretWebApp.Models
{
	public class BussinessUnitModel
	{
		public int ID { get; set; }
		public string Name { get; set; }

		public string  Description { get; set; }

		public BussinessUnitType Type { get; set; }

		public List<ActivityModel> ActivityList { get; set; }

		public List<ServiceModel> ServiceList { get; set; }
	}
}
