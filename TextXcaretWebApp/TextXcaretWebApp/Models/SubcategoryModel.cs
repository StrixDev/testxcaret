﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextXcaretWebApp.Models
{
	public class SubcategoryModel
	{
		public int ID { get; set; }

		public string Name { get; set; }
	}
}
