﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextXcaretWebApp.Enums
{
	public enum BussinessUnitType : int
	{
		Parque = 1, Tour = 2, Hotel = 3
	}
}