﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using TextXcaretWebApp.Models;
using TextXcaretWebApp.DataAccess;

namespace TextXcaretWebApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CatalogsController : ControllerBase
    {
		public IConfiguration Configuration { get; }
		public CatalogsController(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public List<CategoryModel> GetCategories()
		{
			List<CategoryModel> result = new List<CategoryModel>();

			try
			{
				daoCatalogs dao = new daoCatalogs(Configuration);
				result = dao.SelectCategoryList();
			}
			catch (Exception ex)
			{
				string message = ex.Message;
				throw;
			}

			return result;
		}

		[Route("[action]")]
		public List<SubcategoryModel> GetSubcategories(int categoryId)
		{
			List<SubcategoryModel> result = new List<SubcategoryModel>();

			try
			{
				daoCatalogs dao = new daoCatalogs(Configuration);
				result = dao.SelectSubcategoryList(categoryId);
			}
			catch (Exception ex)
			{
				string message = ex.Message;
				throw;
			}

			return result;
		}
	}
}