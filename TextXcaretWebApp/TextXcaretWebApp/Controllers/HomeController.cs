﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

using TextXcaretWebApp.Models;
using TextXcaretWebApp.DataAccess;

namespace TextXcaretWebApp.Controllers
{
    public class HomeController : Controller
    {
		public IConfiguration Configuration { get; }
		private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
			Configuration = configuration;
		}

        public IActionResult Index()
        {
			BussinessUnitModel bussinessUnitModel = new BussinessUnitModel();
			bussinessUnitModel.ID = 1;
			bussinessUnitModel.Name = "Xcaret";

			bussinessUnitModel.ActivityList = new List<ActivityModel>();

			try
			{
				daoActivities dcl = new daoActivities(Configuration);
				bussinessUnitModel.ActivityList = dcl.SelectActivityList(bussinessUnitModel.ID);
			}
			catch (Exception)
			{
				throw;
			}

			return View(bussinessUnitModel);
		}
    }
}
