﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using TextXcaretWebApp.Models;
using TextXcaretWebApp.DataAccess;

namespace TextXcaretWebApp.Controllers
{
    public class AuthController : Controller
    {
		private IConfiguration Configuration { get; }
		private daoAuthorization dao;


		public AuthController(IConfiguration configuration)
		{
			Configuration = configuration;
			dao = new daoAuthorization(Configuration);
		}

		public IActionResult Index()
		{
			AuthorizationModel authorization = new AuthorizationModel();

			try
			{
				authorization.ActivityList = dao.SelectActivityList(null);
			}
			catch (Exception)
			{
				throw;
			}

			return View(authorization);
		}


		public ActionResult Aproove(int id)
		{
			try
			{
				dao.UpdateActivity(requestID : id, userID : 1, binnacleStatusID : 1);
			}
			catch (Exception)
			{
				throw;
			}

			return RedirectToAction("Index");
		}

		public ActionResult Reject(int id)
		{
			try
			{
				dao.UpdateActivity(requestID: id, userID: 1, binnacleStatusID: 2);
			}
			catch (Exception)
			{
				throw;
			}

			return RedirectToAction("Index");
		}
	}
}