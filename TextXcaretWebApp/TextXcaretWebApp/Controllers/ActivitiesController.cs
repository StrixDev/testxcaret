﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using TextXcaretWebApp.Models;
using TextXcaretWebApp.DataAccess;

namespace TextXcaretWebApp.Controllers
{
	public class ActivitiesController : Controller
	{
		private IConfiguration Configuration { get; }
		private daoActivities dao;


		public ActivitiesController(IConfiguration configuration)
		{
			Configuration = configuration;
			dao = new daoActivities(Configuration);
		}

		public IActionResult Index()
		{
			List<ActivityModel> ActivityList = new List<ActivityModel>();

			try
			{
				ActivityList = dao.SelectActivityList(1);
			}
			catch (Exception)
			{
				throw;
			}

			return View(ActivityList);
		}

		public IActionResult Create( int bussinessUnitId, string bussinessUnitName)
		{
			ActivityViewModel activityViewModel = new ActivityViewModel
			{
				BussinesUnitID = bussinessUnitId
				, BussinesUnitName = bussinessUnitName
				, CategoryList = GetCategoryList()
			};

			return View(activityViewModel);
		}

		[HttpPost]
		public IActionResult Create(ActivityViewModel activityViewModel)
		{
			if (ModelState.IsValid)
			{
				try
				{
					dao.InsertActivity(activityViewModel, 2);

				}
				catch (Exception)
				{
					throw;
				}

				return RedirectToAction("Index");
			}
			else
				return View();
		}

		public ActionResult Details(int id)
		{
			ActivityModel activityDetails = new ActivityModel();

			try
			{
				activityDetails = dao.SelectActivityDetail(id);
			}
			catch (Exception)
			{
				throw;
			}

			return View(activityDetails);
		}

		public ActionResult Edit(int id, int bussinessUnitId, string bussinessUnitName)
		{
			ActivityViewModel activityDetails = new ActivityViewModel();

			try
			{
				activityDetails = dao.SelectActivityDetail(id);
				activityDetails.BussinesUnitID = bussinessUnitId;
				activityDetails.BussinesUnitName = bussinessUnitName;
				activityDetails.CategoryList = GetCategoryList();
			}
			catch (Exception)
			{
				throw;
			}

			return View(activityDetails);
		}

		[HttpPost]
		public ActionResult Edit(int id, ActivityModel activity)
		{
			activity.ID = activity.ID == 0 ? id : activity.ID;

			try
			{
				dao.UpdateActivity(activity, 2);
			}
			catch (Exception)
			{
				throw;
			}

			return RedirectToAction("Index");
		}

		private List<CategoryModel> GetCategoryList()
		{
			List<CategoryModel> categoryList = new List<CategoryModel>();

			try
			{
				daoCatalogs dao = new daoCatalogs(Configuration);
				categoryList = dao.SelectCategoryList();
			}
			catch (Exception)
			{
				throw;
			}

			return categoryList;
		}

		public ActionResult Delete(int id)
		{
			ActivityModel activityDetails = new ActivityModel();

			try
			{
				activityDetails = dao.SelectActivityDetail(id);

				activityDetails.Status = 0;

				dao.UpdateActivity(activityDetails, 2);
			}
			catch (Exception)
			{
				throw;
			}

			return RedirectToAction("Index");
		}

		public JsonResult GetSubcategoryList(int categoryId)
		{
			List<SubcategoryModel> result = new List<SubcategoryModel>();

			try
			{
				daoCatalogs dao = new daoCatalogs(Configuration);
				result = dao.SelectSubcategoryList(categoryId);
			}
			catch (Exception ex)
			{
				string message = ex.Message;
				throw;
			}
			
			return Json(result);
		}
	}
}